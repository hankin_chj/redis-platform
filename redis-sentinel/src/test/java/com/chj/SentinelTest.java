package com.chj;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/*
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SentinelTest {
	@Resource
	private SentinelTemplateUtil service;
	@Test
	public void testSet() throws InterruptedException{
		 service.set("hankin", "hankin test");
	     String str = service.get("hankin");
    	 System.out.println("====="	+str);
	}	
	@Test
	public void testGet() throws InterruptedException{
		System.out.println("============="+service.get("age1"));
	}
}
