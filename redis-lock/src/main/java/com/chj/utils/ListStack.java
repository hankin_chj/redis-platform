package com.chj.utils;

import java.util.EmptyStackException;
import java.util.LinkedList;

/**
 * 栈的功能：进栈、出栈、返回栈口元素……
 */
public class ListStack {
    private LinkedList<Object> linkedList=new LinkedList<Object>();
    //1、构造方法
    public ListStack(){}

    //2、出栈
    public Object pop(){
        if(isEmpty()){
            throw new EmptyStackException();
        }
        return linkedList.removeFirst();
    }
    //3、进栈
    public void push(Object obj){ //注意o不要0的区别，不要写成0了
        linkedList.addFirst(obj);
    }
    //4、清空
    public void clear() {
        linkedList.clear();
    }
    //5、判断是否为空
    public boolean isEmpty(){
        return linkedList.isEmpty();
    }
    //6 将对象转换成字符串
    public String toString(){
        return linkedList.toString();
    }
    //7、返回栈口元素
    public Object peek(){
        if(isEmpty()){
            throw new EmptyStackException();
        }
        return linkedList.peekFirst();
    }

    public static void main(String[] args) {
        ListStack stack=new ListStack();
        //进栈
        stack.push("a");
        stack.push("b");
        //出栈
        System.out.println(stack.pop());
        //返回栈口元素
        System.out.println(stack.peek());
    }
}
