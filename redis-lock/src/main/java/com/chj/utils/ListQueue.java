package com.chj.utils;

import java.util.EmptyStackException;
import java.util.LinkedList;

/**
 * 队列的功能：队尾进，队首出
 */
public class ListQueue {
    // 借助LinkedList 类中的方法实现队列
    private LinkedList<Object> linkedList = new LinkedList<Object>();
    // 1、构造方法
    public ListQueue() { }

    // 2、出列
    public Object get() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return linkedList.removeFirst();
    }
    // 3、进列
    public void put(Object obj) {
        linkedList.addLast(obj);
    }
    // 4、清空
    public void clear() {
        linkedList.clear();
    }
    // 5、返回队列首元素(不删除)
    public Object getTop() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return linkedList.peekFirst();
    }
    // 6、将对象转换成字符串
    public String toString() {
        return linkedList.toString();
    }
    // 7、判断队列是否为空
    public boolean isEmpty() {
        return linkedList.isEmpty();
    }

    public static void main(String[] args) {
        ListQueue mq = new ListQueue();
        // 进列
        mq.put("a");
        mq.put("b");
        mq.put("c");
        // 出列
        System.out.println(mq.get());
        System.out.println(mq.get());
        // 返回对首元素
        System.out.println(mq.getTop());
    }
}
