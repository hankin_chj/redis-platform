package com.chj.main;

import redis.clients.jedis.Jedis;

public class Test {
    //eval "return redis.call('get',KEYS[1])" 1 name
    public static void main(String[] args) {
        // 工具类初始化
        Jedis jedis = new Jedis("192.168.30.156",6379);
        jedis.auth("12345678");
        jedis.set("name","hankin");
        String script = "return redis.call('get',KEYS[1])";
        Object result = jedis.eval(script,1,"name");
        System.out.println(result);
        jedis.disconnect();
    }
}
