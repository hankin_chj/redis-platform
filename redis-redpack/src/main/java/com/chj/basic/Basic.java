package com.chj.basic;

public class Basic {
	public static String ip = "192.168.30.156";
	public static int port = 6379;
	public static String auth = "12345678";
	public static int honBaoCount = 1000;

	public static int threadCount = 20;
	public static String hongBaoPoolKey = "hongBaoPoolKey"; //LIST类型来模拟红包池子
	public static String hongBaoDetailListKey = "hongBaoDetailListKey";//LIST类型，记录所有用户抢红包的详情
	public static String userIdRecordKey = "userIdRecordKey";//记录已经抢过红包的用户ID,防止重复抢
	
	/*
	 * KEYS[1]:hongBaoPool：  //键hongBaoPool为List类型，模拟红包池子，用来从红包池抢红包
	 * KEYS[2]:hongBaoDetailList：//键hongBaoDetailList为List类型，记录所有用户抢红包的详情
	 * KEYS[3]:userIdRecord ： //键userIdRecord为Hash类型，记录所有已经抢过红包的用户ID
	 * KEYS[4]:userid ： //模拟抢红包的用户ID
	 * 
	 * jedis.eval(Basic.getHongBaoScript,4, Basic.hongBaoPoolKey, Basic.hongBaoDetailListKey,Basic.userIdRecordKey, userid);
	 *  Lua脚本参数个数  key[1] key[2] key[3] key[4]
	*/
	public static String getHongBaoScript =
			// 查询用户是否抢过红包，如果已经抢过则直接返回nil
			"if redis.call('hexists', KEYS[3], KEYS[4]) ~= 0 then\n"   +
					"return nil\n" +
			"else\n"  +
					// 从红包池取出一个红包
					"local hongBao = redis.call('rpop', KEYS[1]);\n"  +
					// 判断红包池的红包是否为空
					"if hongBao then\n"  +
						 "local x = cjson.decode(hongBao);\n"  +
						// 将红包信息与用户ID信息绑定，表示该用户已经抢到红包
						"x['userId'] = KEYS[4];\n"  +
						"local re = cjson.encode(x);\n"  +
						// 记录该用户已经抢过userIdRecordKey
						"redis.call('hset', KEYS[3], KEYS[4], '1');\n"  +
						// 将抢红包的结果详情存入hongBaoDetailListKey
						"redis.call('lpush', KEYS[2], re);\n" +
						"return re;\n"  +
					"end\n"  +
			"end\n"  +
		    "return nil";
}
