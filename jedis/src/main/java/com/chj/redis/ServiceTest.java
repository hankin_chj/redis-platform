package com.chj.redis;

import com.chj.jedis.RedisTools;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class ServiceTest {
    public static void main(String[] args) {
        RedisTools.initRedisData();
        long t = System.currentTimeMillis();
        // 1128毫秒
//        delNoStus(RedisTools.keys);
        // 32 毫秒
        delNoPipe(RedisTools.keys);
        System.out.println(System.currentTimeMillis()-t);
    }
    // 删除10000条数据
    public static void delNoStus(String...keys){
        Jedis jedis = new Jedis(RedisTools.ip,RedisTools.port);
        for(String key:keys){
            jedis.del(key);
        }
        jedis.close();
    }
    // 使用Pipeline方式删除10000条数据（批量操作）
    public static void delNoPipe(String...keys){
        Jedis jedis = new Jedis(RedisTools.ip,RedisTools.port);
        Pipeline pipelined = jedis.pipelined();
        for(String key:keys){
            pipelined.del(key);//redis?
        }
        pipelined.sync();//
        jedis.close();
    }
}
